import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Main 
{
	public static void main(String[] args) 
	{
		ArrayList<Persona> personas = new ArrayList<Persona>() ;
		
		personas.add(new Persona("Ana",10, 50));
		personas.add(new Persona("Mia",11, 50));		
		personas.add(new Persona("Paula",12, 60));
		personas.add(new Persona("Manuel",28, 40));
		personas.add(new Persona("Sofia",22, 30));

		aplicar(personas, p -> p.mostrarNombre());
		
		aplicar(personas, p -> p.mostrarEdad());
		aplicar(personas, p -> { System.out.println(p.nombre + " " + p.edad); });
		
		filtrar(personas, (p -> p.esMayor()));
		filtrar(personas, p -> { return p.edad > 11; });  // forma equivalente de escribirlo filtrar(personas, p -> p.edad > 11;
		calcular(personas, p -> p.mediaEdad());
		calcular(personas, p -> { return p.edad / 3.0; }); // forma equivalente de escribirlo calcular(personas, p -> p.edad/3.0)
		
		int resultado = sumar(personas, p->p.getEdad() );
		System.out.println("suma edad " + resultado);
		
		int resultado2 = sumar(personas, p->p.getPeso() );
		System.out.println("suma peso " + resultado2);
	}
	
	public static void aplicar(ArrayList<Persona> personas, Consumer<Persona> consumidor)
	{
		for(Persona persona: personas)
			consumidor.accept(persona);
	}
	
	public static void filtrar(ArrayList<Persona> personas, Predicate<Persona> filtro)
	{
		for(Persona persona: personas)
		{
			if (filtro.test(persona)==true)
				persona.mostrarNombre();
		}
	}
	
	public static void calcular(ArrayList<Persona> personas, Function <Persona, Double> funcion)
	{
		for(Persona persona: personas)
			System.out.println(persona.nombre + " " + funcion.apply(persona));
	}
	
	public static int sumar(ArrayList<Persona> personas, Function<Persona, Integer> funcion)
	{
		int ret = 0;
		for (Persona persona: personas)
			ret = ret + funcion.apply(persona);
		return ret;
	}
}
